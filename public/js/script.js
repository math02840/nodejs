const routes = [
    { path: "/", component: Home },
    {
        path: '/items', component: showAllItemsComponent, meta: {
            requireAuth: true
        }
    },
    {
        path: '/signup', component: signUpComponent, meta: {
            isArleadyAuth: true
        }
    },
    {
        path: '/signin', component: signInComponent, meta: {
            isArleadyAuth: true
        }
    },
    { path: '/404', component: notFound },
    { path: '/:catchAll(.*)', redirect: '/404' },
];

const router = VueRouter.createRouter({
    history: VueRouter.createWebHashHistory(),
    routes,
});


const vueApp = {
    computed: {
        loggedIn() {
            return localStorage.getItem("token");
        },
    }
};

const app = Vue.createApp(vueApp);
app.use(router);

router.beforeEach((to, from, next) => {
    if (to.matched.some((record) => record.meta.requireAuth)) {
        if (!localStorage.getItem("token")) {
            next({
                path: "/signin",
            });
        } else {
            next();
        }
    } else if (to.matched.some((record) => record.meta.isArleadyAuth)) {
        if (localStorage.getItem("token")) {
            next({
                path: "/",
            });
        } else {
            next();
        }
    } else {
        next();
    }
});
window.addEventListener("DOMContentLoaded", () => {
    app.mount("#vue_root");
});