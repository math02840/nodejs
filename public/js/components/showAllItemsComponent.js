var showAllItemsComponent = {
    template: `
    <h1>Mon stock</h1>

    <input class="form-control form-control-lg mt-4 mb-4" type="text" v-on:change="search" placeholder="Rechercher">

    <div class="table-responsive">
    <table class="table">
        <thead class="thead-dark">
            <tr>
                <th scope="col">Nom</th>
                <th scope="col">Catégorie</th>
                <th scope="col">Quantité</th>
                <th scope="col">Modifier</th>
                <th scope="col">Supprimer</th>
            </tr>
        </thead>
        <tbody>
            <tr v-for="item in items">
                <th scope="row">{{ item.name }}</th>
                <td>{{ item.category }}</td>
                <td><i class="fas fa-minus mr-1" v-on:click="removeItem(item)"></i> {{ item.quantity }} <i class="fas fa-plus ml-2" v-on:click="addItem(item)"></i></td>
                <td><i class="fas fa-edit" type="button" data-toggle="modal" data-target="#updateModal" v-on:click="setItemToUpdate(item)"></i></td>
                <td><i class="fas fa-trash" v-on:click="deleteItem(item.id)"></i></td>
            </tr>
        </tbody>
        <tfoot>
            <tr>
                <th scope="row" colspan="5"><button type="button" class="btn btn-primary btn-block" data-toggle="modal" data-target="#createModal">Ajouter</button></th>
            </tr>
        </tfoot>
    </table>
    </div>

    <div class="modal fade" id="updateModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Modifier un produit</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <input class="form-control form-control-lg mt-4 mb-4" type="text" v-model="itemToUpdate.name" placeholder="Nom">
                    <input class="form-control form-control-lg mb-4" type="number" v-model="itemToUpdate.quantity" placeholder="Quantité">
                    <select id="inputStateUpdate" class="form-control">
                        <option v-for="category in categories" v-bind:value="category.id">{{ category.name }}</option>
                    </select>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Fermer</button>
                    <button type="button" class="btn btn-primary" v-on:click="submitModif">Sauvegarder les modifications</button>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="createModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Ajouter un produit</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <input class="form-control form-control-lg mt-4 mb-4" type="text" v-model="form.name" placeholder="Nom">
                    <input class="form-control form-control-lg mb-4" type="number" v-model="form.quantity" placeholder="Quantité">
                    <select id="inputState" class="form-control">
                        <option v-for="category in categories" v-bind:value="category.id">{{ category.name }}</option>
                    </select>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Fermer</button>
                    <button type="button" class="btn btn-primary" v-on:click="submitForm">Ajouter le produit</button>
                </div>
            </div>
        </div>
    </div>
  `,
    data() {
        return {
            items: [],
            categories: [],
            form: {
                name: '',
                quantity: '',
                category: ''
            },
            itemToUpdate: {
                id: '',
                name: '',
                quantity: '',
                category: ''
            }
        }
    },
    methods: {
        fetchItems() {
            axios.get('/api/items/user', {
                headers: {
                    Authorization: `Bearer ${localStorage.getItem("token")}`,
                },
            })
                .then(reponse => this.items = reponse.data).catch(erreur => this.items = [{ name: "Erreur de chargement" }]);
        },

        fetchCategories() {
            axios.get('/api/category', {
                headers: {
                    Authorization: `Bearer ${localStorage.getItem("token")}`,
                },
            })
                .then(reponse => this.categories = reponse.data).catch(erreur => this.categories = [{ name: "Erreur de chargement" }]);
        },

        setItemToUpdate(item) {
            this.itemToUpdate.id = item.id;
            this.itemToUpdate.name = item.name;
            this.itemToUpdate.quantity = item.quantity;
            this.itemToUpdate.category = item.category;
        },

        removeItem(item) {
            var itemToRemove = item;
            if (item.quantity == 1) {
                this.deleteItem(itemToRemove.id)
            } else {
                console.log("id item :" + item.id)
                itemToRemove.quantity = itemToRemove.quantity - 1;
                axios.put('/api/items/updateqty/' + item.id, itemToRemove, {
                    headers: {
                        Authorization: `Bearer ${localStorage.getItem("token")}`,
                    },
                })
                    .then((res) => {

                    })
                    .catch((error) => {
                        window.alert("Uen erreur s'est produite lors de la modification : " + error);
                    });
            }
        },

        addItem(item) {
            var itemToAdd = item;
            itemToAdd.quantity = itemToAdd.quantity + 1;
            axios.put('/api/items/updateqty/' + itemToAdd.id, itemToAdd, {
                headers: {
                    Authorization: `Bearer ${localStorage.getItem("token")}`,
                },
            })
                .then((res) => {
                    //location.reload();
                })
                .catch((error) => {
                    window.alert("Une erreur s'est produite lors de la modification : " + error);
                });
        },

        deleteItem(id) {
            swal({
                title: "Êtes-vous sûr ?",
                text: "Une fois supprimé, vous ne pourrez pas récupérer ce produit",
                icon: "warning",
                buttons: true,
                dangerMode: true,
                buttons: ["Annuler", "Supprimer"]
              })
              .then((willDelete) => {
                if (willDelete) {
                    axios.delete('/api/items/' + id, {
                        headers: {
                            Authorization: `Bearer ${localStorage.getItem("token")}`,
                        },
                    })
                    .then(function (response) {
                            console.log(response)
                            location.reload();
                    })
                    .catch(function (error) {
                            console.log(error);
                    });
                } else {
                  swal("Votre produit n'est pas supprimé !");
                }
              });
        },

        submitForm() {
            var e = document.getElementById("inputState");
            this.form.category = e.value;
            if (this.form.name == '' || this.form.quantity == '' || this.form.category == '') {
                swal("Erreur", 'Veuillez remplir tous les champs', "error");
            } else {
            axios.post('/api/items/create', this.form, {
                headers: {
                    Authorization: `Bearer ${localStorage.getItem("token")}`,
                },
            })
                .then((res) => {
                    location.reload();
                })
                .catch((error) => {
                    window.alert("Une erreur s'est produite lors de l'ajout du produit : " + error);
                });
            }
        },

        submitModif() {
            var e = document.getElementById("inputStateUpdate");
            var itemToUpdate = {
                name: this.itemToUpdate.name,
                quantity: this.itemToUpdate.quantity,
                category: e.value
            }

            if (itemToUpdate.name == '' || itemToUpdate.quantity == '' || itemToUpdate.category == '') {
                swal("Erreur", 'Veuillez remplir tous les champs', "error");
            } else {
            axios.put('/api/items/' + this.itemToUpdate.id, itemToUpdate, {
                headers: {
                    Authorization: `Bearer ${localStorage.getItem("token")}`,
                },
            })
                .then((res) => {
                    location.reload();
                })
                .catch((error) => {
                    window.alert("Une erreur s'est produite lors de la modification : " + error);
                });
            }
        },

        search: function (evt) {
            console.log(evt.target.value);
            var search = evt.target.value
            if (search == '') {
                this.fetchItems();
            } else {
                axios.post('/api/items/search',
                    { search: search }, {
                    headers: {
                        Authorization: `Bearer ${localStorage.getItem("token")}`,
                    },
                }
                )
                    .then((res) => {
                        console.log(res);
                        this.items = res.data;
                    })
                    .catch((error) => {
                        window.alert("Une erreur s'est produite lors de la recherce du produit : " + error);
                    });
            }
        }

    },
    mounted() {
        this.fetchItems()
        this.fetchCategories()
    }
};