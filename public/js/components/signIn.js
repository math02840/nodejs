var signInComponent = {
    template: `
    <div class="container my-5">
        <section>
            <h3 class="font-weight-normal text-center dark-grey-text pb-2 display-4"><strong>Se connecter</strong></h3>
            <hr class="w-header my-4">
            <div class="row d-flex justify-content-center">
                <div class="col-6">
                    <div class="md-form md-outline form-lg">
                        <input type="mail" v-model="formSignIn.emailsignin" class="form-control form-control-lg" placeholder="Email">
                    </div>
                    <div class="md-form md-outline form-lg mt-3">
                        <input type="password" v-model="formSignIn.passwordsignin" class="form-control form-control-lg" placeholder="Mot de passe">
                    </div>
                    <button class="btn btn-block btn-primary btn-lg mt-3" v-on:click="submitForm">Se connecter</button>
                </div>
            </div>
        </section>
    </div>
  `,
    data() {
        return {
            formSignIn: {
                emailsignin: '',
                passwordsignin: '',
            }
        }
    },
    methods: {
        submitForm() {
            if (this.formSignIn.emailsignin == '' || this.formSignIn.passwordsignin == '') {
                swal("Erreur", 'Veuillez remplir tous les champs', "error");
            } else {
                axios.post('/api/user/login', this.formSignIn)
                    .then((res) => {
                        if (res.data == 'Aucun utilisateur n\'est lié à cet email') {
                            swal("Erreur", res.data, "error");
                        } else if (res.data == 'Mot de passe incorrect') {
                            swal("Erreur", res.data, "error");
                        } else {
                            const token = res.data;
                            swal({
                                title: "Bienvenue !",
                                text: "Vous êtes à présent connecté !",
                                icon: "success",
                                button: "OK",
                              })
                              .then( clickOk => {
                                localStorage.setItem('token', token);
                                this.$router.push({ path: "/" });
                                this.$router.go();
                              });
                        }
                    })
                    .catch((error) => {
                        swal("Erreur", "Une erreur s'est produite lors de la connexion au compte : " + error, "error");
                    });
            }
        }
    }
};