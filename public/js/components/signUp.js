var signUpComponent = {
    template: `
    <div class="container my-5">
        <section>
            <h3 class="font-weight-normal text-center dark-grey-text pb-2 display-4"><strong>S'inscrire</strong></h3>
            <hr class="w-header my-4">
            <div class="row d-flex justify-content-center">
                <div class="col-6">
                    <div class="md-form md-outline form-lg">
                        <input type="text" v-model="form.usernamesignup" class="form-control form-control-lg" placeholder="Nom d'utilisateur">
                    </div>
                    <div class="md-form md-outline form-lg mt-3">
                        <input type="mail" v-model="form.emailsignup" class="form-control form-control-lg" placeholder="Email">
                    </div>
                    <div class="md-form md-outline form-lg mt-3">
                        <input type="password" v-model="form.passwordsignup" class="form-control form-control-lg" placeholder="Mot de passe">
                    </div>
                    <div class="md-form md-outline form-lg mt-3">
                        <input type="password" v-model="repeatedPass" class="form-control form-control-lg" placeholder="Retapez le mot de passe">
                    </div>
                    <button class="btn btn-block btn-primary btn-lg mt-3" v-on:click="submitForm">S'inscrire</button>
                </div>
            </div>
        </section>
    </div>
  `,
    data() {
        return{
            form: {
                usernamesignup: '',
                emailsignup: '',
                passwordsignup: '',
            },
            repeatedPass: '',
        }
    },
    methods: {
            submitForm(){
                if (this.form.usernamesignup == '' || this.form.emailsignup == '' || this.form.passwordsignup == '') {
                    swal("Erreur", 'Veuillez remplir tous les champs', "error");
                } else if (this.form.passwordsignup != this.repeatedPass) {
                    swal("Erreur", 'Les mots de passe sont différents', "error");
                } else {
                        axios.post('/api/user/create', this.form)
                        .then((res) => {
                            if (res.data == 'Un utilisateur est déjà associé à cette adresse email !') {
                                swal("Erreur", res.data, "error");
                            } else {
                                swal({
                                    title: "Félicitations",
                                    text: "Le compte a été créé avec succès",
                                    icon: "success",
                                    button: "OK",
                                  })
                                  .then( clickOk => {
                                    this.$router.push({ path: "/signin" });
                                  });                                
                            }
                        })
                        .catch((error) => {
                            swal("Erreur", "Une erreur s'est produite lors de la création du compte : " + error, "error");
                        });
                    }
                }
            }
};