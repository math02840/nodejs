var Home = {
    template: `
    <div class="container my-5">
      <section class="dark-grey-text text-center">
        <h3 class="font-weight-bold mb-4 pb-2">Notre projet</h3>
        <p class="text-muted w-responsive mx-auto mb-5">Chaque année, des miliers de produits sont perdus ou jetés (date de péremption dépassé, légumes perdus...). Nous avons aujourd'hui décidé de mettre à votre disposition BestStock !</p>
        <div class="row">
          <div class="col-md-6 mb-4">
            <div class="view overlay rounded z-depth-1">
              <img src="https://mdbootstrap.com/img/Photos/Others/laptop-sm.jpg" class="img-fluid" alt="Sample project image">
              <a>
                <div class="mask rgba-white-slight"></div>
              </a>
            </div>
            <div class="px-3 pt-3 mx-1 mt-1 pb-0">
              <a class="orange-text">
                <h6 class="font-weight-bold mt-2 mb-3"><i class="fas fa-chart-line pr-2"></i>Economie</h6>
              </a>
              <h4 class="font-weight-bold mb-3">Fini les achats inutiles</h4>
              <p class="text-muted">Vous ne perdrez plus vos produits, en ayant une vue sur tout ce que vous avez, vous pourrez suivre votre stock avant de racheter ou afin de savoir si une date de péremption se rapproche.</p>
            </div>
          </div>
          <div class="col-md-6 mb-4">
            <div class="view overlay rounded z-depth-1">
              <img src="https://www.toutpratique.com/img/cms_post/5851/big.jpg" class="img-fluid" alt="Sample project image">
              <a>
                <div class="mask rgba-white-slight"></div>
              </a>
            </div>
            <div class="px-3 pt-3 mx-1 mt-3 pb-0">
              <a class="blue-text">
                <h6 class="font-weight-bold mt-2 mb-3"><i class="fas fa-eye pr-2"></i>Visibilité</h6>
              </a>
              <h4 class="font-weight-bold mb-3">Voyez tout vos produits en un clin d'oeil</h4>
              <p class="text-muted">En vous rendant sur votre liste, vous pourrez, en seulement quelques secondes voir tout ce dont vous disposez afin de ne plus gaspiller !</p>
            </div>
          </div>
        </div>
      </section>
    </div>
  `,
};