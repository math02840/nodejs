const sql = require("../services/database");

const Item = function(item) {
    this.name = item.name;
    this.quantity = item.quantity;
    this.category = item.category;
    this.userId = item.userId
  };

  Item.create = (newItem, result) => {
    sql.query("INSERT INTO item SET ?", newItem, (err, res) => {
      if (err) {
        console.log("error: ", err);
        result(err, null);
        return;
      }
  
      console.log("created item: ", { id: res.insertId, ...newItem });
      result(null, { id: res.insertId, ...newItem });
    });
  };

  Item.findById = (itemId, result) => {
    sql.query(`SELECT * FROM item WHERE id = ${itemId}`, (err, res) => {
      if (err) {
        console.log("error: ", err);
        result(err, null);
        return;
      }
  
      if (res.length) {
        console.log("found item: ", res[0]);
        result(null, res[0]);
        return;
      }
  
      result({ kind: "not_found" }, null);
    });
  };

  Item.findByName = (itemName, result) => {
    sql.query(`SELECT item.id, item.name, item.quantity, category.name AS 'category' FROM item JOIN category ON item.category = category.id WHERE item.name LIKE '%${itemName.name}%' AND item.userId = ${itemName.userId}`, (err, res) => {
      if (err) {
        console.log("error: ", err);
        result(err, null);
        return;
      }
  
      console.log("items: ", res);
      result(null, res);
    });
  };

  Item.findByUser = (userId, result) => {
    sql.query(`SELECT item.id, item.name, item.quantity, category.name AS 'category' FROM item JOIN category ON item.category = category.id WHERE item.userId = ${userId}`, (err, res) => {
      if (err) {
        console.log("error: ", err);
        result(null, err);
        return;
      }
  
      console.log("items: ", res);
      result(null, res);
    });
  };
  
  Item.getAll = result => {
    sql.query("SELECT item.id, item.name, item.quantity, category.name AS 'category' FROM item JOIN category ON item.category = category.id", (err, res) => {
      if (err) {
        console.log("error: ", err);
        result(null, err);
        return;
      }
  
      console.log("items: ", res);
      result(null, res);
    });
  };
  
  Item.updateById = (id, item, result) => {
    sql.query(
      "UPDATE item SET name = ?, quantity = ?, category = ? WHERE id = ?",
      [item.name, item.quantity, item.category, id],
      (err, res) => {
        if (err) {
          console.log("error: ", err);
          result(null, err);
          return;
        }
  
        if (res.affectedRows == 0) {
          result({ kind: "not_found" }, null);
          return;
        }
  
        console.log("updated item: ", { id: id, ...item });
        result(null, { id: id, ...item });
      }
    );
  };
  
  Item.remove = (id, result) => {
    sql.query("DELETE FROM item WHERE id = ?", id, (err, res) => {
      if (err) {
        console.log("error: ", err);
        result(null, err);
        return;
      }
  
      if (res.affectedRows == 0) {
        result({ kind: "not_found" }, null);
        return;
      }
  
      console.log("deleted item with id: ", id);
      result(null, res);
    });
  };

  Item.updateQty = (id, item, result) => {
    sql.query(
      "UPDATE item SET quantity = ? WHERE id = ?",
      [item.quantity, id],
      (err, res) => {
        if (err) {
          console.log("error: ", err);
          result(null, err);
          return;
        }
  
        if (res.affectedRows == 0) {
          result({ kind: "not_found" }, null);
          return;
        }
  
        console.log("updated item: ", { id: id, ...item });
        result(null, { id: id, ...item });
      }
    );
  };
  
  Item.removeAll = result => {
    sql.query("DELETE FROM item", (err, res) => {
      if (err) {
        console.log("error: ", err);
        result(null, err);
        return;
      }
  
      console.log(`deleted ${res.affectedRows} items`);
      result(null, res);
    });
  };
  
  module.exports = Item;
  
