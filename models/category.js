const sql = require("../services/database");

const Category = function(category) {
    this.name = item.name;
  };

  Category.findById = (categoryId, result) => {
    sql.query(`SELECT * FROM category WHERE id = ${categoryId}`, (err, res) => {
      if (err) {
        console.log("error: ", err);
        result(err, null);
        return;
      }
  
      if (res.length) {
        console.log("found category: ", res[0]);
        result(null, res[0]);
        return;
      }
  
      result({ kind: "not_found" }, null);
    });
  };
  
  Category.getAll = result => {
    sql.query("SELECT * FROM category", (err, res) => {
      if (err) {
        console.log("error: ", err);
        result(null, err);
        return;
      }
  
      console.log("categories: ", res);
      result(null, res);
    });
  };
  
  module.exports = Category;