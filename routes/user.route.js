module.exports = app => {
    const user = require("../controllers/user.controller");

app.post("/api/user/login", user.login);

app.post("/api/user/create", user.create);

app.get("/api/users", user.findAll);

};