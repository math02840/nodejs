module.exports = app => {
    const category = require("../controllers/category.controller");  
    app.get("/api/category", category.findAll);
    app.get("/api/category/:categoryId", category.findOne);
  };