module.exports = app => {
  const item = require("../controllers/item.controller");
  const token = require("./../services/token")

  app.post("/api/items/create", token.tokenVerify, item.create);

  app.post("/api/items/search", token.tokenVerify, item.search);

  app.get("/api/items", token.tokenVerify, item.findAll);

  app.get("/api/items/user", token.tokenVerify, item.findByUser);

  app.get("/api/items/:itemId", token.tokenVerify, item.findOne);

  app.put("/api/items/:itemId", token.tokenVerify, item.update);

  app.put("/api/items/updateqty/:itemId", token.tokenVerify, item.updateQty);

  app.delete("/api/items/:itemId", token.tokenVerify, item.delete);

  app.delete("/api/items", token.tokenVerify, item.deleteAll);

};