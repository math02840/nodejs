require("dotenv").config();
module.exports = {
	"database": {
		"host": process.env.DB_HOST,
		"port": process.env.DB_PORT,
		"db": process.env.DB_DATABASE,
		"user": process.env.DB_USER,
		"password": process.env.DB_PASS
	},
	"token": {
		"secret": process.env.TOKEN_SECRET
	}
}