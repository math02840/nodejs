const Item = require("../models/item");
const security = require("../services/token")
const Token = require("./../services/token")
exports.create = (req, res) => {
    if (!req.body) {
      res.status(400).send({
        message: "Content can not be empty!"
      });
    }

    const authHeader = req.headers["authorization"];
    const token = authHeader && authHeader.split(" ")[1];

    const tokenObject = Token.parseJwt(token);
  
    const item = new Item({
      name: req.body.name,
      quantity: req.body.quantity,
      category: req.body.category,
      userId: tokenObject.user.id
    });
  
    Item.create(item, (err, data) => {
      if (err)
        res.status(500).send({
          message:
            err.message || "Some error occurred while creating the item."
        });
      else res.send(data);
    });
  };

exports.findAll = (req, res) => {
    Item.getAll((err, data) => {
      if (err)
        res.status(500).send({
          message:
            err.message || "Some error occurred while retrieving items."
        });
      else res.send(data);
    });
  };

  exports.findByUser = (req, res) => {

    const authHeader = req.headers["authorization"];
    const token = authHeader && authHeader.split(" ")[1];

    const tokenObject = Token.parseJwt(token);

    Item.findByUser(tokenObject.user.id, (err, data) => {
      if (err)
        res.status(500).send({
          message:
            err.message || "Some error occurred while retrieving items."
        });
      else res.send(data);
    });
  };

exports.findOne = (req, res) => {
    Item.findById(req.params.itemId, (err, data) => {
      if (err) {
        if (err.kind === "not_found") {
          res.status(404).send({
            message: `Not found item with id ${req.params.itemId}.`
          });
        } else {
          res.status(500).send({
            message: "Error retrieving item with id " + req.params.itemId
          });
        }
      } else res.send(data);
    });
  };

exports.update = (req, res) => {
    if (!req.body) {
      res.status(400).send({
        message: "Content can not be empty!"
      });
    }
  
    Item.updateById(
      req.params.itemId,
      new Item(req.body),
      (err, data) => {
        if (err) {
          if (err.kind === "not_found") {
            res.status(404).send({
              message: `Not found item with id ${req.params.itemId}.`
            });
          } else {
            res.status(500).send({
              message: "Error updating item with id " + req.params.itemId
            });
          }
        } else res.send(data);
      }
    );
  };

  exports.updateQty = (req, res) => {
    if (!req.body) {
      res.status(400).send({
        message: "Content can not be empty!"
      });
    }
  
    Item.updateQty(
      req.params.itemId,
      new Item(req.body),
      (err, data) => {
        if (err) {
          if (err.kind === "not_found") {
            res.status(404).send({
              message: `Not found item with id ${req.params.itemId}.`
            });
          } else {
            res.status(500).send({
              message: "Error updating item with id " + req.params.itemId
            });
          }
        } else res.send(data);
      }
    );
  };

  exports.search = (req, res) => {
    if (!req.body) {
      res.status(400).send({
        message: "Content can not be empty!"
      });
    }

    const authHeader = req.headers["authorization"];
    const token = authHeader && authHeader.split(" ")[1];

    const tokenObject = Token.parseJwt(token);

    const search = {
      name: req.body.search,
      userId: tokenObject.user.id
    }

    console.log("Recherche " + search);

    Item.findByName(search, (err, data) => {
      res.send(data);
    });
  };

exports.delete = (req, res) => {
    Item.remove(req.params.itemId, (err, data) => {
      if (err) {
        if (err.kind === "not_found") {
          res.status(404).send({
            message: `Not found item with id ${req.params.itemId}.`
          });
        } else {
          res.status(500).send({
            message: "Could not delete item with id " + req.params.itemId
          });
        }
      } else res.send({ message: `Item was deleted successfully!` });
    });
  };

exports.deleteAll = (req, res) => {
    Item.removeAll((err, data) => {
      if (err)
        res.status(500).send({
          message:
            err.message || "Some error occurred while removing all blocks."
        });
      else res.send({ message: `All items were deleted successfully!` });
    });
  };