const User = require("../models/user");
var bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');
const saltRounds = 12;
const dotenv = require('dotenv');

dotenv.config();

process.env.TOKEN_SECRET;

function generateAccessToken(user) {
  return jwt.sign({user}, process.env.TOKEN_SECRET, { expiresIn: '1800s' });
}

exports.create = (req, res) => {
  if (!req.body) {
    res.status(400).send({
      message: "Content can not be empty!"
    });
  }
  console.log("----------------- ", req.body)

  bcrypt.hash(req.body.passwordsignup, saltRounds, function (err, hash) {

    const newUser = new User({
      name: req.body.usernamesignup,
      mail: req.body.emailsignup,
      pass: hash
    })

    User.findByMail(req.body.emailsignup, (err, user) => {
      if(user) {
        res.send('Un utilisateur est déjà associé à cette adresse email !');
        return;
      } else {
        User.create(newUser, (err, data) => {
          if (err)
            res.status(500).send({
              message:
                err.message || "Some error occurred while creating the user."
            });
          else res.send(data);
        });
      }
    });
  });
};

exports.login = (req, res) => {
  if (!req.body) {
    res.status(400).send({
      message: "Content can not be empty!"
    });
  }
  console.log("----------------- ", req.body)

  User.findByMail(req.body.emailsignin, (err, user) => {
    if (err) { res.send('Aucun utilisateur n\'est lié à cet email'); }
    else {
      if (!user) {
        res.send('Aucun utilisateur n\'est lié à cet email');
      } else {
        bcrypt.compare(req.body.passwordsignin, user.pass, function (err, result) {
          if (result) {
            const token = generateAccessToken(user);
            console.log(token);
            res.json(token);
          } else {
            res.send('Mot de passe incorrect');
          }
        });
      }
    }
  });
}

exports.findAll = (req, res) => {
  User.getAll((err, data) => {
    if (err)
      res.status(500).send({
        message:
          err.message || "Some error occurred while retrieving users."
      });
    else res.send(data);
  });
};
