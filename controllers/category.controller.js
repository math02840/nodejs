const Category = require("../models/category");

exports.findAll = (req, res) => {
    Category.getAll((err, data) => {
      if (err)
        res.status(500).send({
          message:
            err.message || "Some error occurred while retrieving categories."
        });
      else res.send(data);
    });
  };

exports.findOne = (req, res) => {
    Category.findById(req.params.categoryId, (err, data) => {
      if (err) {
        if (err.kind === "not_found") {
          res.status(404).send({
            message: `Not found category with id ${req.params.categoryId}.`
          });
        } else {
          res.status(500).send({
            message: "Error retrieving category with id " + req.params.categoryId
          });
        }
      } else res.send(data);
    });
  };