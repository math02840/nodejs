const express = require("express");
const bodyParser = require("body-parser");
const path = require('path')

const app = express();

app.use(bodyParser.json());

app.use(bodyParser.urlencoded({ extended: true }));

app.use((req, res, next) => {
  res.setHeader('Access-Control-Allow-Origin', '*');
  res.setHeader('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content, Accept, Content-Type, Authorization');
  res.setHeader('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE, PATCH, OPTIONS');
  next();
});

require("./routes/user.route")(app);
require("./routes/item.route")(app);
require("./routes/category.route")(app);

const HTML_DIR = path.join(__dirname, '/public/')
app.use(express.static(HTML_DIR))

app.listen(3000, () => {
  console.log("Server is running on port 3000.");
});